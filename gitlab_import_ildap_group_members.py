#!/usr/bin/env python

import ldap, gitlab, getpass, argparse

def get_gitlab_group(gl, groupname):
    gl_groups = gl.groups.search(groupname)
    if len(gl_groups) != 1:
        raise Exception("number of gitlab groups %s = %i" % (groupname, len(gl_groups)))
    return gl_groups[0]

def get_ildap_group_members(ildap, groupname):
    result_id = ildap.search('ou=People,dc=inria,dc=fr', ldap.SCOPE_SUBTREE, filterstr="(inriaGroupMemberOf=cn=%s,ou=groups,dc=inria,dc=fr)" % (groupname,))
    result_type, result_data = ildap.result(result_id)
    return result_data

def get_gitlab_user(gl, ldap_attrs):
    gl_users = gl.users.search(ldap_attrs['mail'][0])
    users_found = []
    for u in gl_users:
        if u.email.lower() == ldap_attrs['mail'][0].lower():
            for i in u.identities:
                if 'extern_uid' in i and 'provider' in i:
                    if i['extern_uid'] == "uid=%s,ou=people,dc=inria,dc=fr" % (ldap_attrs['uid'][0]) and i['provider'] == 'ldapmain':
                        users_found.append(u)
    if len(users_found) > 1:
        raise Exception("number of git users with email %s = %i" % (email, len(users_found)))
    if len(users_found) == 0:
        return None
    return users_found[0]
    
def add_ildap_members_to_gitlab(ildap_groupname,
                                gitlab_url,
                                gitlab_token,
                                gitlab_groupname,
                                level,
                                dry_run):
    gl = gitlab.Gitlab(gitlab_url, gitlab_token)
    print("connected to %s" % gitlab_url)
    ildap = ldap.initialize('ldap://ildap.inria.fr')
    print("connected to ildap")

    gl_group = get_gitlab_group(gl, gitlab_groupname)
    print("got gitlab group %s, has %i members" % (gl_group.name, len(gl_group.members.list())))
    gl_group_members = gl_group.members.list()
    ildap_members = get_ildap_group_members(ildap, ildap_groupname)
    print("ildap group %s has %i members" % (ildap_groupname, len(ildap_members)))

    print("iterating ildap group members and adding those existing in gitlab but not yet in the gitlab group to the gitlab group, with access level %s" % (level,))
    
    for (dn, attrs) in ildap_members:
        gl_user = get_gitlab_user(gl, attrs)
        if gl_user:
            print("  found gitlab user: %s / %s, matching ildap group member: %s" % (gl_user.username, gl_user.email, attrs['inriaLogin'][0]))
            already_in_group = False
            for gl_group_member in gl_group_members:
                if gl_user.id == gl_group_member.id:
                    print "  already in gitlab group"
                    already_in_group = True
                    break
            if not already_in_group:
                if dry_run:
                    print("  would create member with user_id %s, level %s" % (gl_user.id, level))
                else:
                    print("  create member with user_id %s, level %s" % (gl_user.id, level))
                    gl_new_group_member = gl_group.members.create({'user_id': gl_user.id,
                                                                   'access_level': level})

if __name__ == "__main__":
    level_choices = ['GUEST', 'REPORTER', 'DEVELOPER', 'MASTER', 'OWNER']
    parser = argparse.ArgumentParser(description='add INRIA ildap group members to a gitlab group')
    parser.add_argument('ildap_group', type=str,
                        help='ildap group name')
    parser.add_argument('gitlab_url', type=str,
                        help='gitlab instance url')
    parser.add_argument('gitlab_group', type=str,
                        help='gitlab group name')
    parser.add_argument('level', type=str,
                        help='access level granted to the members added to the gitlab group',
                        choices=level_choices)
    parser.add_argument('--dry_run', action='store_true')
    args = parser.parse_args()
    level = [gitlab.GUEST_ACCESS,
             gitlab.REPORTER_ACCESS,
             gitlab.DEVELOPER_ACCESS,
             gitlab.MASTER_ACCESS,
             gitlab.OWNER_ACCESS][level_choices.index(args.level)]
    gitlab_token = getpass.getpass("%s private token?" % (args.gitlab_url,))
    add_ildap_members_to_gitlab(args.ildap_group,
                                args.gitlab_url,
                                gitlab_token,
                                args.gitlab_group,
                                level,
                                args.dry_run)
