gitlab_import_ildap_group_members
=================================

This python script scans the members of an INRIA ildap group and adds
the matching gitlab users to a gitlab group.

- It makes sure that the matching between the ildap account and the
  gitlab account is exact, not only based on the username: it ensures
  that the authentication identity of the gitlab account matches
  exactly the ildap identity. Thus, though it could theorically work
  with any gitlab instance, it will in practice only work with the
  inria gitlab instance, which uses the inria ildap identity for
  authentication.

- It takes an access level as parameter (see
  https://docs.gitlab.com/ee/user/permissions.html). This group access
  level is given to all users added to the group.

- If a user is already member of the group, it does
  nothing. Specifically, it does *not* update the access level of
  members already in the gitlab group.

- It will only add to the gitlab group existing gitlab users. Note
  that an inria member needs to connect at least one time to the
  gitlab so that his/her gitlab account is created.

- There is an option to run the script in 'dry_run' mode, so see what
  would be done without actually doing nothing.

Prerequisites
-------------

- python 2.7

- python-ldap (debian package)

- python-gitlab (from pip) (not the python-gitlab debian package which
  is something else and is outdated, do not use it)

- being in the inria network (or use the inria vpn) to have access to
  the ildap. We mean the national INRIA ildap
  ``ldap://ildap.inria.fr``, *not* a local ldap.

- knowing the INRIA ildap groupname that you want. Eg, if you want to
  search the ildap groups to find your team:

        $ ldapsearch -x -H "ldap://ildap.inria.fr" -b "ou=groups,dc=inria,dc=fr" | grep -i <team name>

  If you want to check that it's the right group, you can list its
  members:

        $ ldapsearch -x -H "ldap://ildap.inria.fr" -b "ou=People,dc=inria,dc=fr" "(inriaGroupMemberOf=cn=<ldap group name>,ou=groups,dc=inria,dc=fr)"

- having a gitlab access token (you can generate one in your gitlab
  profile settings, in the Access Tokens tab). The access token will
  be interactively asked when you run the script.

Usage and parameters
--------------------

    $ ./gitlab_import_ildap_group_members.py --help
    usage: gitlab_import_ildap_group_members.py [-h] [--dry_run]
                                                ildap_group gitlab_url
                                                gitlab_group
                                                {GUEST,REPORTER,DEVELOPER,MASTER,OWNER}

    add INRIA ildap group members to a gitlab group

    positional arguments:
      ildap_group           ildap group name
      gitlab_url            gitlab instance url
      gitlab_group          gitlab group name
      {GUEST,REPORTER,DEVELOPER,MASTER,OWNER}
                            access level granted to the members added to the
                            gitlab group

    optional arguments:
      -h, --help            show this help message and exit
      --dry_run

Example:
--------

Add all members of the ildap group SED-ral to the gitlab group sedral
with access level DEVELOPER:

    $ ./gitlab_import_ildap_group_members.py SED-ral https://gitlab.inria.fr sedral DEVELOPER
